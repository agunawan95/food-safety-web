from flask import *

app = Flask(__name__)

@app.route("/")
def main():
    page_name = 'Track & Trace'
    return render_template('home.html', page_name=page_name)

@app.route("/trace")
def trace():
    page_name = 'Track & Trace'
    return render_template('view-track-trace.html', page_name=page_name)

@app.route("/product")
def product_detail():
    page_name = 'Product Detail'
    return render_template('view-product-detail.html', page_name=page_name)

@app.route("/slide/a/description")
def project_description():
    page_name = 'Project Description'
    return render_template('intro-a-project-description.html', page_name=page_name)
    
@app.route("/slide/a/background")
def introductionA():
    page_name = 'Privacy Enhanced and Secure Data Store wit Traceability Using Blockchain'
    return render_template('intro-a-background.html', page_name=page_name)

@app.route("/slide/a/objective")
def objective():
    page_name = 'Objective'
    return render_template('intro-a-objective.html', page_name=page_name)

@app.route("/slide/a/approach")
def approach():
    page_name = 'Approach'
    return render_template('intro-a-approach.html', page_name=page_name)

@app.route("/slide/a/architecture")
def architecture_a():
    page_name = 'Platform Architecture'
    return render_template('intro-a-architecture.html', page_name=page_name)

@app.route("/slide/a/ssi")
def ssi():
    page_name = 'Self-Sovereign Identity Implementation and Results'
    return render_template('intro-a-ssi.html', page_name=page_name)

@app.route("/slide/b/introduction")
def introductionB():
    page_name = 'Supply Chain Traceability With Global Collaboration'
    return render_template('intro-b.html', page_name=page_name)

@app.route("/slide/b/gs1")
def gs1():
    page_name = 'GS1 Standard'
    return render_template('intro-b-gs1.html', page_name=page_name)

@app.route("/slide/b/architecture")
def architecture_b():
    page_name = 'Architecture'
    return render_template('intro-b-architecture.html', page_name=page_name)

@app.route("/slide/c/introduction")
def introductionC():
    page_name = 'Consumer-Centric Trustable Food with Traceability'
    return render_template('intro-c.html', page_name=page_name)

@app.route("/slide/c/case")
def case():
    page_name = 'Demonstration Cases'
    return render_template('intro-c-demo-case.html', page_name=page_name)

@app.route("/slide/credit")
def credit():
    page_name = 'Owner Credit'
    return render_template('view-owner-credit.html', page_name=page_name)

@app.route("/slide/last")
def last():
    page_name = 'Take Home Message'
    return render_template('intro-take-home-msg.html', page_name=page_name)

@app.route("/manage/track-trace")
def manage_track_trace():
    page_name = 'Add Track & Trace Data'
    return render_template('manage-track-trace.html', page_name=page_name)

@app.route("/manage/recall")
def manage_recall():
    page_name = 'Add Recall Data'
    return render_template('manage-recall.html', page_name=page_name)

@app.route("/manage/import")
def manage_import():
    page_name = 'Add Import Data'
    return render_template('manage-import.html', page_name=page_name)

@app.route("/manage/product")
def manage_product():
    page_name = 'Upload/Change Transaction Data'
    return render_template('manage-product.html', page_name=page_name)

@app.route('/template/upload', methods=['GET'])
def download_csv_template():
    return send_from_directory('csv_template', 'template.csv')

@app.route("/qr/product")
def demo_product():
    page_name = 'Product QR'
    return render_template('view-demo-product.html', page_name=page_name)

@app.route("/case/product")
def demo_situation_product():
    page_name = 'Case Introduction'
    return render_template('intro-case-situation.html', page_name=page_name)

@app.route("/qr/trace")
def demo_trace():
    page_name = 'Track & Trace QR'
    return render_template('view-demo-trace.html', page_name=page_name)

if __name__ == '__main__':
    app.run(debug=True)